#!/usr/bin/env python3
import winrm
import os
import sys
import socket

server = os.getenv('server')
service = os.getenv('service')
user = os.getenv('user')
password = os.getenv('password')
list_service = service.split(',')
#list_service = str(service)
service_restart = str(sys.argv[1])

def check_server_is_alived(server):
    port = 135

    args = socket.getaddrinfo(server, port, socket.AF_INET, socket.SOCK_STREAM)
    for family, socktype, proto, canonname, sockaddr in args:
        s = socket.socket(family, socktype, proto)
        try:
            s.connect(sockaddr)
        except socket.error:
            return False
        else:
            s.close()
            return True

def check_service():
    if service_restart in list_service:
        pass
    else:
        print("El servicio no existe, debe ingresar un servicio valido. Esta es la lista de servicios")
        for service in list_service:
            print(service)
            
        sys.exit(1)


def restart_service():
    ps_script = """Restart-Service '{}'""".format(service_restart)
    s = winrm.Session(server, auth=(user, password), transport='ntlm')
    r = s.run_ps(ps_script)
    if r.status_code == 0:
        print("{} fue reiniciado correctamente".format(service_restart))
    else:
        print("El reinicio fallo, favor contactar a operaciones CCO")
    #print(r.status_code)
    #print(r.std_out)


if check_server_is_alived(server):
    print("El servidor {} esta running".format(server))
else:
    print("El servidor esta apagado, o con problemas. Favor revisar")
    sys.exit()

check_service()
restart_service()
